'use strict';

angular.module('roboguide')
  .service('Storage', function Storage() {
    var api,
      clientCreds = {
        orgName:'filipovsky',
        appName:'youguide'
      };
    api = new Apigee.Client(clientCreds);

    return {
      routeList: function (query) {
        var opts = { endpoint: 'routes' };

        if (query) {
          opts.qs = {
            ql: "select * where city = '" + query + "'"
          };
        }

        return Q.ninvoke(api, 'request', opts).then(function (result) {
          console.log('Query result', query, result);
          return result.entities;
        });
      },

      routeById: function (id) {
        return Q.ninvoke(api, 'getEntity', { type: 'route', uuid: id })
          .then(function (result) {
            return result[0]._data;
          });
      },

      pointsByRoute: function (route) {
        return Q.ninvoke(api, 'request', {
          endpoint: 'points',
          qs: { ql: 'select * where route = "' + route.uuid + '"' }
        }).then(function (result) {
          return result.entities;
        });
      }
    };
  });
