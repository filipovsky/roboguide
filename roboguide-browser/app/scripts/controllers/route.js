'use strict';

angular.module('roboguide')
  .controller('RouteCtrl', function ($scope, $stateParams, Storage) {

    Q.all([
      Storage.routeById($stateParams.id),
      Storage.pointsByRoute({ uuid: $stateParams.id })
    ]).spread(function (route, points) {
        var bounds = new google.maps.LatLngBounds();

        $scope.routeMarkers = [];
        $scope.map = new google.maps.Map(document.getElementById('map'), {
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        $scope.panorama = $scope.map.getStreetView();

        $scope.me = new google.maps.Marker({
          clickable: false,
          icon: {
            url: './images/glyphicons/png/glyphicons_034_old_man.png',
            size: new google.maps.Size(32,32)
          },
          shadow: null,
          zIndex: 999,
          map: $scope.map
        });

        navigator.geolocation.watchPosition(function (result) {
          var latLng = result.coords;
          $scope.me.setPosition(latLng);
        });

        _.forEach(points, function (point) {
          var latLng = new google.maps.LatLng(point.lat, point.lng),
              index = point.index,
              marker;

          if (!index) { return; }

          marker = new google.maps.Marker({
            position: latLng,
            map: $scope.map,
            title: point.name,
            point: point,
            icon: {
              url: './images/map_icons/numbers/number_' + index + '.png',
              size: new google.maps.Size(32,37)
            },
          });

          $scope.routeMarkers.push(marker);
          google.maps.event.addListener(marker, 'click', function () {
            var marker = this;

            if (!marker.point.index) { return; }

            _.forEach($scope.routeMarkers, function (toDisable) {
              if (!toDisable.point.index) { return; }
              toDisable.setIcon(
                './images/map_icons/numbers/number_' +
                toDisable.point.index + '.png');
            });

            marker.setIcon(
              './images/map_icons/numbers_active/number_' +
              marker.point.index + '.png');

            if (!$scope.$$phase) {
              $scope.$apply(function () {
                $scope.point = marker.point;
              });
            } else {
              $scope.point = marker.point;
            }

          });
          bounds.extend(latLng);
          google.maps.event.trigger(_.first($scope.routeMarkers), 'click');
        });

        $scope.map.fitBounds(bounds);
      }).done();


    $scope.$watch('point', function () {
      if (!$scope.point) { return; }
      var point = $scope.point;
      $scope.media = null;

      if (point.audio && $scope.isPhoneGap()) {
        $scope.media = new Media(point.audio, function () {
          $scope.$apply(function () { $scope.playing = false; });
        });
      }
    });

    $scope.play = function () {
      if (!$scope.media) { return; }
      $scope.playing = true;
      $scope.media.play();
    };

    $scope.pause = function () {
      $scope.playing = false;
      $scope.media.pause();
    };

    $scope.playCls = function () {
      return $scope.media ? '' : 'disabled';
    };

    $scope.canGoBack = function () {
      if (!$scope.point) { return; }

      var index = $scope.point.index,
          canGoBack;

      canGoBack = _.any($scope.routeMarkers, function (marker) {
        return marker.point.index < index;
      });

      return canGoBack;
    };

    $scope.canGoForward = function () {
      if (!$scope.point) { return; }

      var index = $scope.point.index,
          canGoForward;

      canGoForward = _.any($scope.routeMarkers, function (marker) {
        return marker.point.index > index;
      });

      return canGoForward;
    };

    $scope.goBack = function () {
      _.any($scope.routeMarkers, function (marker) {
        if (marker.point.index === $scope.point.index - 1) {
          if ($scope.playing) {
            $scope.media.stop();
            $scope.playing = false;
          }
          google.maps.event.trigger(marker, 'click');
          return true;
        }
      });
    };

    $scope.goForward = function () {
      _.any($scope.routeMarkers, function (marker) {
        if (marker.point.index === $scope.point.index + 1) {
          if ($scope.playing) {
            $scope.media.stop();
            $scope.playing = false;
          }
          google.maps.event.trigger(marker, 'click');
          return true;
        }
      });
    };

    $scope.canShowPanorama = function () {
      if (!$scope.point) { return false; }
      return !!$scope.point.heading;
    };

    $scope.togglePanorama = function () {
      var point = $scope.point,
          panorama = $scope.panorama;

      if (panorama.getVisible()) {
        return panorama.setVisible(false);
      }

      if (!panorama.getVisible()) {
        panorama.setPosition(new google.maps.LatLng(point.lat, point.lng));

        panorama.setPov({
          heading: point.heading,
          pitch: point.pitch || 0
        });

        panorama.setZoom(point.zoom || 0);

        panorama.setVisible(true);
      }
    };

  });
