'use strict';

angular.module('roboguide')
  .controller('NavCtrl', function ($scope, $state, $stateParams) {
    $scope.query = $stateParams.query;

    $scope.main = ($state.current.name === 'main');

    $scope.upBtnCls = function () {
      if (!$scope.main) { return 'show-up-btn'; }
      return '';
    };

    $scope.search = function () {
      $state.go('main', { query: $scope.query });
    };

  });
