'use strict';

angular.module('roboguide')
  .controller('MainCtrl', function ($scope, $state, $stateParams, Storage) {

    $scope.active = null;
    $scope.lastLocation = null;
    $scope.query = $stateParams.query;

    // function showCurrentLocation() {
    //   var q = Q.defer();

    //   _.forEach($scope.routeMarkers, function (marker) {
    //     marker.setMap(null);
    //   });

    //   navigator.geolocation.getCurrentPosition(function (result) {
    //     var latLng = result.coords;
    //     $scope.map.setCenter(
    //       new google.maps.LatLng(
    //         latLng.latitude,
    //         latLng.longitude));
    //     $scope.map.setZoom(15);
    //     q.resolve(latLng);
    //   });

    //   return q.promise;
    // }

    // function showRoute(route) {
    //   var bounds = new google.maps.LatLngBounds();

    //   $scope.routeMarkers = [];
    //   Storage.pointsByRoute(route).
    //     then(function (points) {
    //       _.forEach(points, function (point) {
    //         var latLng = new google.maps.LatLng(point.lat, point.lng),
    //             index = point.index;

    //         if (!index) { return; }

    //         $scope.routeMarkers.push(new google.maps.Marker({
    //           position: latLng,
    //           map: $scope.map,
    //           title: point.name,
    //           clickable: false,
    //           icon: {
    //             url: './images/map_icons/numbers/number_' + index + '.png',
    //             size: new google.maps.Size(32,37)
    //           }
    //         }));

    //         bounds.extend(latLng);
    //       });

    //       $scope.map.fitBounds(bounds);
    //     });

    // }

    // function drawMap() {
    //   $scope.map = new google.maps.Map(document.getElementById('map'), {
    //     center: new google.maps.LatLng(55.796, 49.109),
    //     zoom: 15,
    //     mapTypeId: google.maps.MapTypeId.ROADMAP
    //   });

    //   $scope.me = new google.maps.Marker({
    //     clickable: false,
    //     icon: {
    //       url: './images/glyphicons/png/glyphicons_034_old_man.png',
    //       size: new google.maps.Size(32,32)
    //     },
    //     shadow: null,
    //     zIndex: 999,
    //     map: $scope.map
    //   });

    //   showCurrentLocation().then(function (coords) {
    //     $scope.search(coords);
    //   });

    //   navigator.geolocation.watchPosition(function (result) {
    //     var latLng = result.coords;
    //     $scope.lastLocation = new google.maps.LatLng(
    //         latLng.latitude, latLng.longitude);
    //     $scope.me.setPosition($scope.lastLocation);
    //   });
    // }

    $scope.$on('$viewContentLoaded', function () {
      if ($scope.isPhoneGap()) {
        //window.document.addEventListener('deviceready', drawMap);
      } else {
        //drawMap();
      }
    });

    $scope.notEmptyInputCls = function () {
      if ($scope.query) { return 'input-group'; }
    };

    $scope.activeRouteCls = function (route) {
      if ($scope.active === route) { return 'active'; }
    };

    $scope.toggleActive = function (route) {
      // if ($scope.active === route) {
      //   showCurrentLocation();
      //   $scope.active = null;
      //   if ($scope.lastLocation) {
      //     $scope.map.panTo($scope.lastLocation);
      //     $scope.map.setZoom(15);
      //   }
      //   return;
      // }
      $scope.active = route;
      // showRoute(route);
    };

    $scope.goldStarClass = function (i, route) {
      if (i <= route.rate) { return 'gold'; }
    };

    $scope.goToRoute = function (route, $event) {
      console.log(route);
      $event.stopPropagation();
      $state.go('route', { id: route.uuid });
    };

    $scope.searching = true;
    Storage.routeList($stateParams.query).then(function (routes) {
      $scope.$apply(function () {
        $scope.routes = routes;
        $scope.searching = false;
      });
    });

  });
