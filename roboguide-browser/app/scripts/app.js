'use strict';

var app = angular.module('roboguide', [ 'ui.router', 'ngTouch', 'ngSanitize' ])
  .config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('main', {
        url: '/?query',
        views: {
          'content': {
            templateUrl: 'views/main.html',
            controller: 'MainCtrl'
          },
          'nav': {
            templateUrl: 'views/nav.html',
            controller: 'NavCtrl'
          }
        }
      })
      .state('route', {
        url: '/routes/:id',
        views: {
          'content': {
            templateUrl: 'views/route.html',
            controller: 'RouteCtrl'
          },
          'nav': {
            templateUrl: 'views/nav.html',
            controller: 'NavCtrl'
          }
        }
      });
  });

app.run(function ($rootScope) {
  $rootScope.isPhoneGap = function () {
    return (window.cordova || window.PhoneGap || window.phonegap) &&
            /^file:\/{3}[^\/]/i.test(window.location.href) &&
            /ios|iphone|ipod|ipad|android/i.test(navigator.userAgent);
  };

  $rootScope.GOOGLE_API_KEY = 'AIzaSyD1uGuvTuhtjeJz7fwzgxsMwFjbzgYklyI';
});
